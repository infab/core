# Changelog

All notable changes to `infab\core` will be documented in this file

## 1.3.1
- Fix naming for creation of api tests
- Change name of function in tests in stub
- Remove order by sortindex from stub

## 1.3.0
- New method for determining which relationships should be eager loaded. Main benefit of this is that we no longer have to define realtionships in the controller which was kind of weird.
```php
    // New method
    // Model
    const RELATIONSHIPS = [
        'tags', 'language'
    ];

    // Controller
    $eagerLoad = $this->getEagerLoad(Contact::RELATIONSHIPS);
```
```php
    // Old method
    // In controller
    /**
     * Defines which relationships that can be included
     * @var array
     */
    public $possibleRelationships = [
        'tags'   => 'tags',
        'images' => 'images'
    ];

    $eagerLoad = $this->getEagerLoad(Contact::RELATIONSHIPS);
```

## 1.2.10
- Generated API-tests now has the correct modelnames

## 1.2.9
- Add support for Laravel 5.7

## 1.2.8
- Add `setMeta()` method

## 1.2.4
- Pluarlize filenames for created feature tests

## 1.2.3
- test on php 7.3
- add libzip-dev to `gitlab-ci.yml`

## 1.2.2
- update return type for tests

## 1.2.1
- bump laravel version to 5.8

## 1.2.0
- add make command for api controller tests
- add test for pagination

## 1.0.1
- add exception, better test coverage
