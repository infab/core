# infab/core

[![Latest Version on Packagist](https://img.shields.io/packagist/v/infab/core.svg)](https://packagist.org/packages/infab/core)
[![Total Downloads](https://img.shields.io/packagist/dt/infab/core.svg)](https://packagist.org/packages/infab/core)


[![pipeline status](https://gitlab.com/infab/core/badges/master/pipeline.svg)](https://gitlab.com/infab/core/commits/master)
[![pipeline status](https://gitlab.com/infab/core/badges/master/pipeline.svg)](https://gitlab.com/infab/core/commits/master)
[![coverage report](https://gitlab.com/infab/core/badges/master/coverage.svg)](https://gitlab.com/infab/core/commits/master)

**Core functionality for Fabriq CMS**

<!-- [![Latest Stable Version](https://poser.pugx.org/infab/core/v/stable)](https://packagist.org/packages/infab/core)
[![Total Downloads](https://poser.pugx.org/infab/core/downloads)](https://packagist.org/packages/infab/core)
[![License](https://poser.pugx.org/infab/core/license)](https://packagist.org/packages/infab/core) -->
