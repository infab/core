<?php

namespace Infab\Core\Test;

use Infab\Core\Test\TestCase;

class RepositoryMakeCommandTest extends TestCase
{
    public function setUp() :void
    {
        parent::setUp();
        $path = base_path();
        $path .= '/app/Repositories';
        exec("rm -rf {$path}");
        $testsPath = base_path() . '/tests';
        exec("rm -rf {$testsPath}");
    }

    /** @test **/
    public function it_can_create_a_repository_from_stub()
    {
       $this->artisan('make:repository-interface', [
            'name' => 'ContactRepositoryInterface',
            '--model' => 'Contact'
        ])->assertExitCode(0);
    }

    /** @test **/
    public function it_can_create_an_eloquent_repo_from_stub()
    {
        $this->artisan('make:eloquent-repository', [
            'name' => 'EloquentContactRepository',
            '--model' => 'Contact'
        ])->assertExitCode(0);
    }

    /** @test **/
    public function it_can_create_a_caching_repository_from_stub()
    {
        $this->artisan('make:caching-repository', [
            'name' => 'CachingContactRepository',
            '--model' => 'Contact'
        ])->assertExitCode(0);
    }

    /** @test **/
    public function it_can_create_all_parts_in_one_go()
    {
        $path = base_path();
        $path .= '/app/Repositories';

        $this->artisan('make:repository', [
            'name' => 'Contact',
            '--model' => 'Contact',
            '--with-cache' => true
        ])->assertExitCode(0);
        $this->assertFileExists($path . '/EloquentContactRepository.php');
        $this->assertFileExists($path . '/Decorators/CachingContactRepository.php');
        $this->assertFileExists($path . '/Interfaces/ContactRepositoryInterface.php');
    }
}
