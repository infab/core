<?php

namespace Infab\Core\Test\_fixtures_;

use League\Fractal\TransformerAbstract;

class BookTransformer extends TransformerAbstract
{
    public function transform($book)
    {
        return [
            'id'    => (int) $book['id'],
            'name' => 'Fixed status',
            'transformed_thing' => (int) $book['id'] + 10
        ];
    }
}
