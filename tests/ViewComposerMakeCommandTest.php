<?php

namespace Infab\Core\Test;

use Mockery as m;
use League\Fractal;
use League\Fractal\Manager;
use Infab\Core\Test\TestCase;

class ViewComposerMakeCommandTest extends TestCase
{
    public function setUp() :void
    {
        parent::setUp();
        $path = base_path();
        $path .= '/app/Http/ViewComposers';
        exec("rm -rf {$path}");
    }

    /** @test **/
    public function it_can_create_a_view_composer_from_stub()
    {
        $this->artisan('make:view-composer', [
            'name' => 'ContactsComposer',
            '--model' => 'Contact'
        ])->assertExitCode(0);
    }

    /**
     * @test
     */
    public function it_will_throw_an_exception_if_no_model_is_specified()
    {
        // Arrange
        $this->expectException(\InvalidArgumentException::class);
        \Artisan::call('make:view-composer', [
            'name' => 'ContactsComposer',
        ]);
    }

    /** @test **/
    public function it_will_replace_backslashes_if_the_model_option_has_it()
    {
        $this->artisan('make:view-composer', [
            'name' => 'ContactsComposer',
            '--model' => '\Contact'
        ])->assertExitCode(0);
    }
}
