<?php

namespace Infab\Core\Test;

use Infab\Core\Test\TestCase;

class ApiControllerMakeCommandTest extends TestCase
{
    public function setUp() :void
    {
        parent::setUp();
        $path = base_path();
        $path .= '/app/Http/Controllers/Api';
        exec("rm -rf {$path}");
        $testsPath = base_path() . '/tests';
        exec("rm -rf {$testsPath}");
    }

    /** @test **/
    public function it_can_create_an_api_controller_from_stub()
    {
       $this->artisan('make:api-controller', [
            'name' => 'ContactsController',
            '--model' => 'Contact'
        ])->assertExitCode(0);
    }

    /** @test **/
    public function it_can_create_api_tests()
    {
        // Arrange
       $this->artisan('make:api-controller-tests', [
            'name' => 'ContactsFeatureTest',
            '--model' => 'Contact',
        ])->assertExitCode(0);
    }

    /** @test **/
    public function it_can_create_an_api_controller_with_tests_from_stub()
    {
        $this->artisan('make:api-controller', [
            'name' => 'ProducerCountiesController',
            '--model' => 'Producer',
            '--tests' => true
        ])->assertExitCode(0);
    }
}
