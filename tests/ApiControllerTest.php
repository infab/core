<?php

namespace Infab\Core\Test;

use Mockery as m;
use League\Fractal;
use League\Fractal\Manager;
use Infab\Core\Test\TestCase;
use Infab\Core\Http\Controllers\Api\ApiController;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Infab\Core\DataTransformer;
use Infab\Core\Test\_fixtures_\BookTransformer;
use League\Fractal\Scope;

class ApiControllerTest extends TestCase
{
    protected $api;

    protected $availableFields = ['id', 'name'];

    protected $requestedFieldsets = ['data' => ['id', 'name', 'banana']];

    public $possibleRelationships = ['images' => 'images'];


    public function setUp() :void
    {
        parent::setUp();
        $manager = new Manager();
        $request = new \Illuminate\Http\Request();
        $dataTransformer = new DataTransformer;
        $request->replace(['number' => 10, 'field' => 'id,name,transformed_thing', 'include' => 'images']);
        $this->api = new ApiController($manager, $dataTransformer, $request);
        $this->api->availableFields = $this->availableFields;
        $this->api->requestedFieldsets = $this->requestedFieldsets;
        // $this->api->possibleRelationships = $this->possibleRelationships;
    }

    /** @test **/
    public function it_can_return_the_status_code()
    {
        // Act
        $code = $this->api->getStatusCode();

        // Assert
        $this->assertEquals($code, 200);
    }

    /** @test */
    public function it_can_set_a_status_code()
    {
        $code = 404;
        $newCode = $this->api->setStatuscode($code);

        $this->assertEquals($code, $this->api->getStatusCode());
    }

    /** @test */
    public function it_can_intersect_available_fields_with_requested_fields()
    {
        $expected = ['id', 'name'];
        $this->assertEquals($this->api->permittedFields(), $expected);
    }

    /** @test */
    public function it_will_return_a_asterisk_string_if_no_fields_are_requested()
    {
        $this->api->requestedFieldsets = ['data' => []];
        $expected = '*';
        $this->assertEquals($this->api->permittedFields(), $expected);
    }

    /** @test **/
    public function it_can_return_unauthorized_response()
    {
        $response = $this->api->errorUnauthorized();
        $this->assertEquals(json_decode($response->content(), true), [
            'error' => [
                'code' => 'GEN-MAYBGTFO',
                'http_code' => 401,
                'message' => 'Unauthorized'
            ]
        ]);
    }

    /** @test **/
    public function it_can_return_forbidden_response()
    {
        $response = $this->api->errorForbidden('Get lost plz');
        $this->assertEquals(json_decode($response->content(), true), [
            'error' => [
                'code' => $this->api::$CODE_FORBIDDEN,
                'http_code' => 403,
                'message' => 'Get lost plz'
            ]
        ]);
    }

    /** @test **/
    public function it_can_return_resource_not_found()
    {
        $response = $this->api->errorNotFound();
        $this->assertEquals(json_decode($response->content(), true), [
            'error' => [
                'code' => $this->api::$CODE_NOT_FOUND,
                'http_code' => 404,
                'message' => 'Resource Not Found'
            ]
        ]);
    }

    /** @test **/
    public function it_can_return_wrong_args_response()
    {
        $response = $this->api->errorWrongArgs();
        $this->assertEquals(json_decode($response->content(), true), [
            'error' => [
                'code' => $this->api::$CODE_WRONG_ARGS,
                'http_code' => 400,
                'message' => 'Wrong Arguments'
            ]
        ]);
    }

    /** @test **/
    public function it_can_parse_eager_load_params()
    {
        $request = new \Illuminate\Http\Request();
        $request->replace(['include' => 'images']);

        $this->api->possibleRelationships  = $this->possibleRelationships;
        $response = $this->api->eagerLoad($request);
        $this->assertEquals($response, [
            0 => "images"
        ]);
    }

    /** @test **/
    public function it_will_squak_if_an_error_is_sent_with_200_ok_response()
    {
        // Arrange
        $this->api->setStatuscode(200);
        $this->expectException(\Infab\Core\Exceptions\InvalidResponseCode::class);

        // Act
        $this->api->respondWithError('This is bad you', 200);
    }

    /** @test **/
    public function it_can_return_internal_error_response()
    {
        $response = $this->api->errorInternalError();
        $this->assertEquals(json_decode($response->content(), true), [
            'error' => [
                'code' => $this->api::$CODE_INTERNAL_ERROR,
                'http_code' => 500,
                'message' => 'Internal Error'
            ]
        ]);
        $this->assertEquals($response->status(), 500);
    }

    /** @test **/
    public function it_can_respond_with_success()
    {
        $response = $this->api->respondWithSuccess('Great success');
        $this->assertEquals(json_decode($response->content(), true), [
            'code' => 'GEN-NOICE',
            'http_code' => 200,
            'message' => 'Great success'
        ]);
    }

    /** @test **/
    public function it_can_respond_with_a_custom_error()
    {
        $response = $this->api->setStatusCode(404)->respondWithError('Oh noe, my customs', 404);
        $this->assertEquals(json_decode($response->content(), true), [
            'error' => [
                'code' => 404,
                'http_code' => 404,
                'message' => 'Oh noe, my customs'
            ]
        ]);
    }


    /** @test **/
    public function it_can_respond_with_a_single_item()
    {
        // Arrange
        $book = collect(['id' => 55]);

        // Act
        $response = $this->api->respondWithItem($book->toArray(), new \Infab\Core\Test\_fixtures_\BookTransformer());

        // Assert
        $this->assertEquals(200, $response->status());
        $this->assertArrayHasKey('data', json_decode($response->content(), true));
        $this->assertEquals(55, json_decode($response->content(), true)['data']['id']);
    }

    /** @test **/
    public function it_can_respond_with_a_collection()
    {
        // Arrange
        $books = collect([['id' => 55], ['id' => 56]]);

        // Act
        $response = $this->api->respondWithCollection($books->toArray(), new \Infab\Core\Test\_fixtures_\BookTransformer());

        // Assert
        $this->assertEquals(200, $response->status());
        $this->assertArrayHasKey('data', json_decode($response->content(), true));
        $this->assertEquals(55, json_decode($response->content(), true)['data'][0]['id']);
        $this->assertEquals(56, json_decode($response->content(), true)['data'][1]['id']);
    }

    /** @test **/
    public function it_can_respond_with_a_paginator()
    {
        // Arrange
        $book1 = new \Infab\Core\Test\_fixtures_\Book();
        $book2 = new \Infab\Core\Test\_fixtures_\Book();
        $books = new \Illuminate\Database\Eloquent\Collection;
        $books->add($book1);
        $books = collect([['id' => 55], ['id' => 56]]);

        $paginator = new Paginator($books, 5, 1);
        // dd($paginator);
        // Act
        $response = $this->api->respondWithPaginator($paginator, new \Infab\Core\Test\_fixtures_\BookTransformer());

        // Assert
        $this->assertEquals(200, $response->status());
        $this->assertArrayHasKey('data', json_decode($response->content(), true));
        $this->assertEquals(55, json_decode($response->content(), true)['data'][0]['id']);
        $this->assertArrayHasKey('meta', json_decode($response->content(), true));
    }

    /** @test **/
    public function it_can_set_meta_on_collection_response()
    {
        // Arrange
        $books = collect([['id' => 55], ['id' => 56]]);
        $this->api->setMeta(['meta_thing' => true]);

        // Act
        $response = $this->api->respondWithCollection($books->toArray(), new \Infab\Core\Test\_fixtures_\BookTransformer());

        // Assert
        $this->assertEquals(200, $response->status());
        $this->assertTrue(json_decode($response->content(), true)['meta']['meta_thing']);
    }

    /** @test **/
    public function it_can_set_meta_on_item_response()
    {
        $book = collect(['id' => 55]);
        $this->api->setMeta(['meta_thing' => true]);

        // Act
        $response = $this->api->respondWithItem($book->toArray(), new \Infab\Core\Test\_fixtures_\BookTransformer());

        // Assert
        $this->assertEquals(200, $response->status());
        $this->assertArrayHasKey('data', json_decode($response->content(), true));
        $this->assertEquals(55, json_decode($response->content(), true)['data']['id']);
        $this->assertTrue(json_decode($response->content(), true)['meta']['meta_thing']);
    }

    /** @test **/
    public function it_can_respond_with_a_paginator_with_meta()
    {
        // Arrange
        $book1 = new \Infab\Core\Test\_fixtures_\Book();
        $book2 = new \Infab\Core\Test\_fixtures_\Book();
        $books = new \Illuminate\Database\Eloquent\Collection;
        $books->add($book1);
        $books = collect([['id' => 55], ['id' => 56]]);

        $paginator = new Paginator($books, 5, 1);
        $this->api->setMeta(['meta_thing' => true]);
        // dd($paginator);
        // Act
        $response = $this->api->respondWithPaginator($paginator, new \Infab\Core\Test\_fixtures_\BookTransformer());

        // Assert
        $this->assertEquals(200, $response->status());
        $this->assertArrayHasKey('data', json_decode($response->content(), true));
        $this->assertEquals(55, json_decode($response->content(), true)['data'][0]['id']);
        $this->assertArrayHasKey('meta', json_decode($response->content(), true));
        $this->assertTrue(json_decode($response->content(), true)['meta']['meta_thing']);
    }

    /** @test **/
    public function it_wont_include_a_meta_key_if_no_meta_is_set()
    {
        $book = collect(['id' => 55]);

        // Act
        $response = $this->api->respondWithItem($book->toArray(), new \Infab\Core\Test\_fixtures_\BookTransformer());

        // Assert
        $this->assertEquals(200, $response->status());
        $this->assertArrayHasKey('data', json_decode($response->content(), true));
        $this->assertArrayNotHasKey('meta', json_decode($response->content(), true));
    }

    /** @test **/
    public function it_can_parse_return_an_array_of_eager_loadable_relations()
    {
        $request = new \Illuminate\Http\Request();
        $request->replace(['include' => 'images']);

        $response = $this->api->getEagerLoad(['images']);
        $this->assertEquals($response, [
            0 => "images"
        ]);
    }

    /** @test **/
    public function it_can_generate_a_transformed_collection()
    {
        // Act
        $this->markTestSkipped('Weirdly not working');
        $book1 = new \Infab\Core\Test\_fixtures_\Book();
        $collection = $this->api->getCollection($book1, new \Infab\Core\Test\_fixtures_\BookTransformer());

        // Assert
        $this->assertInstanceOf(Scope::class, $collection);
        $this->assertEquals($collection->toArray()['data'][0], ['id' => 0, 'name' => 'Fixed status', 'transformed_thing' => 10]);
    }

    /** @test **/
    // public function it_can_generate_a_transformed_single_item()
    // {
    //     // Act
    //     $book1 = new \Infab\Core\Test\_fixtures_\Book();
    //     $item = $this->api->getItem($book1, new \Infab\Core\Test\_fixtures_\BookTransformer());

    //     // Assert
    //     $this->assertInstanceOf(Scope::class, $item);
    //     $this->assertEquals($item->toArray()['data'], ['id' => 0, 'name' => 'Fixed status', 'transformed_thing' => 10]);
    // }
}
