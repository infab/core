<?php

namespace Infab\Core\Test;

use Mockery as m;
use League\Fractal;
use League\Fractal\Manager;
use Infab\Core\Test\TestCase;

class TransformerMakeCommandTest extends TestCase
{
    public function setUp() :void
    {
        parent::setUp();
        $path = base_path();
        $path .= '/app/Transformers';
        exec("rm -rf {$path}");
    }

    /** @test **/
    public function it_can_create_a_transformer_from_stub()
    {
       $this->artisan('make:transformer', [
            'name' => 'ContactTransformer',
            '--model' => 'Contact'
        ])->assertExitCode(0);
    }
}
