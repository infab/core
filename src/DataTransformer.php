<?php

namespace Infab\Core;

use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\Scope;

class DataTransformer
{
    protected $fractal;

    public function __construct()
    {
        $this->fractal = new Manager();
    }

    public function setIncludes(string $includes) : void
    {
        $this->fractal->parseIncludes($includes);
    }

    public function getRequestedIncludes() : array
    {
       return $this->fractal->getRequestedIncludes();
    }

    public function setRequestedFields(string $fields) : void
    {
        $this->fractal->parseFieldsets(['data' => $fields]);
    }

    public function getRequestedFieldsets() : array
    {
        return $this->fractal->getRequestedFieldsets();
    }

    /**
     * Gets a single transformed item
     *
     * @param array $collection
     * @param object $callback
     * @return Item
     */
    public function getItem($collection, $callback) : Item
    {
        return new Item($collection, $callback, 'data');
    }

    /**
     * Gets a transformed collection
     *
     * @param array $collection
     * @param object $callback
     * @return Collection
     */
    public function getCollection($collection, $callback) : Collection
    {
        return new Collection($collection, $callback, 'data');
    }

    /**
     * Get the transformed data
     *
     * @param mixed $resource
     * @return Scope
     */
    public function createData($resource) : Scope
    {
        return $this->fractal->createData($resource);
    }

    /**
     * Sets meta data to the resource
     *
     * @param object $resource
     * @return void
     */
    protected function setupMetaData($resource) : void
    {
        if (! empty($this->meta)) {
            $resource->setMeta($this->meta);
        }
    }

}
