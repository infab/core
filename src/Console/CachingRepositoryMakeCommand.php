<?php

namespace Infab\Core\Console;

use Illuminate\Console\GeneratorCommand;
use Infab\Core\Console\ReplacesModelName;
use Symfony\Component\Console\Input\InputOption;

class CachingRepositoryMakeCommand extends GeneratorCommand
{
    use ReplacesModelName;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'make:caching-repository';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new repository interface';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'RepositoryInterface';

    /**
     * Build the class with the given name.
     *
     * @param  string  $name
     * @return string
     */
    protected function buildClass($name)
    {
        $stub = $this->replaceUserNamespace(
            parent::buildClass($name)
        );

        $stub = $this->files->get($this->getStub());

        $model = $this->option('model') ?? null;
        $stub = $this->replaceNamespace($stub, $name)->replaceClass($stub, $name);

        return $model ? $this->replaceModel($stub, $model) : $stub;
    }

    /**
     * Replace the User model namespace.
     *
     * @param  string  $stub
     * @return string
     */
    protected function replaceUserNamespace($stub)
    {
        return str_replace(
            $this->rootNamespace().'User',
            config('auth.providers.users.model'),
            $stub
        );
    }


    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['model', 'm', InputOption::VALUE_OPTIONAL, 'The model that the repository applies to.']
        ];
    }


    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return __DIR__.'/stubs/caching-repository.stub';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\Repositories\Decorators';
    }
}
