<?php

namespace Infab\Core\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;

class RepositoryMakeCommand extends Command
{

    /**
     * The console command signature
     *
     * @var string
     */
    protected $signature = 'make:repository {name : The prefix of the files to be created } {--model=} {--with-cache}';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new repository';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Repository';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $model = $this->option('model') ?? null;

        $name = $this->argument('name');

        // $withCache =  $this->choice('With cache decorator?', ['no', 'yes'], 0);

        $this->call('make:repository-interface', [
            'name' => "{$name}RepositoryInterface",
            '--model' => $model,
        ]);

        $this->info("Creating App/Repositories/Interfaces/{$name}RepositoryInterface");
        $this->info('Done!');
        $this->info(PHP_EOL);
        $this->info(PHP_EOL);

        $this->call('make:eloquent-repository', [
            'name' => "Eloquent{$name}Repository",
            '--model' => $model,

        ]);
        $this->info("Creating App/Repositories/Eloquent{$name}Repository");
        $this->info('Done!');
        $this->info(PHP_EOL);
        $this->info(PHP_EOL);

        if($this->option('with-cache')) {
            $this->call('make:caching-repository', [
                'name' => "Caching{$name}Repository",
                '--model' => $model,
            ]);
            $this->info("Creating App/Repositories/Decorators/Caching{$name}Repository");
            $this->info('Done!');
            $this->info(PHP_EOL);
            $this->info(PHP_EOL);
        }

        $this->info('Repository created successfully');

        $this->info(PHP_EOL);
        $this->info('Do not forget to register the repo in the AppServiceProvider');


        return 0;
    }
}
