<?php

namespace Infab\Core\Console;

use InvalidArgumentException;
use Illuminate\Console\GeneratorCommand;
use Infab\Core\Console\ReplacesModelName;
use Symfony\Component\Console\Input\InputOption;

class ViewComposerMakeCommand extends GeneratorCommand
{
    use ReplacesModelName;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'make:view-composer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new ViewComposer class';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'ViewComposer';

    /**
     * Build the class with the given name.
     *
     * @param  string  $name
     * @return string
     */
    protected function buildClass($name)
    {
        $stub = $this->replaceUserNamespace(
            parent::buildClass($name)
        );

        if(! $this->option('model')) {
            throw new InvalidArgumentException("The --model option is required");
        }
        $model = $this->option('model');

        return $model ? $this->replaceModel($stub, $model) : $stub;
    }
    
    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['model', 'm', InputOption::VALUE_OPTIONAL, 'The model that the ViewComposer applies to.'],
        ];
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return __DIR__.'/stubs/view-composer.stub';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\Http\ViewComposers';
    }
    
    /**
     * Replace the User model namespace.
     *
     * @param  string  $stub
     * @return string
     */
    protected function replaceUserNamespace($stub)
    {
        return str_replace(
            $this->rootNamespace().'User',
            config('auth.providers.users.model'),
            $stub
        );
    }
}
