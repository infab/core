<?php
namespace Infab\Core\Http\Controllers\Api;

use Illuminate\Routing\Controller as BaseController;
use Infab\Core\Traits\ApiControllerTrait;

class ApiController extends BaseController
{
    use ApiControllerTrait;
}
