<?php
namespace Infab\Core\Traits;

use League\Fractal\Scope;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Manager;
use Infab\Core\Exceptions\InvalidResponseCode;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\Pagination\LengthAwarePaginator as LengthAwarePaginatorContract;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Infab\Core\DataTransformer;
use Infab\Core\Paginators\IlluminateSimplePaginatorAdapter;

trait ApiControllerTrait
{
    /**
     * Default number of records to fetch
     * @var integer
     */
    protected $number = 100;

    /**
     * Maximum number to display without pagination
     * @var integer
     */
    protected $maxNumber = 5000;

    public $requestedFieldsets = ['data' => []];

    /**
     * Statuscode defaults to 200
     * @var integer
     */
    protected $statusCode = 200;

    public $possibleRelationships = [];

    protected $meta = [];

    static $CODE_WRONG_ARGS = "GEN-FUBARGS";
    static $CODE_NOT_FOUND = "GEN-LIKETHEWIND";
    static $CODE_INTERNAL_ERROR = "GEN-AAAGGH";
    static $CODE_UNAUTHORIZED = "GEN-MAYBGTFO";
    static $CODE_FORBIDDEN = "GEN-GTFO";
    static $CODE_INVALID_MIME_TYPE = "GEN-UMWUT";
    static $CODE_SUCCESS = "GEN-NOICE";

    /**
     * Setup for fractal manager
     *
     * @param Manager $fractal
     * @param DataTransformer $dataTransformer
     * @param Request $request
     */
    public function __construct(Manager $fractal, DataTransformer $dataTransformer, Request $request)
    {
        $this->fractal = $fractal;
        $this->request = $request;
        $this->dataTransformer = $dataTransformer;
        // Are we going to try and include embedded data?
        if ($request->has('include')) {
            $this->dataTransformer->setIncludes($request->input('include'));
        }

        if ($request->has('number')) {
            if ($request->input('number') <= $this->maxNumber) {
                $this->number = $request->input('number');
            }
        }

        if ($request->has('field')) {
            $this->dataTransformer->setRequestedFields($request->input('field'));
            $this->requestedFieldsets = $this->dataTransformer->getRequestedFieldsets();
        }
    }

    /**
     * Getter for statusCode
     *
     * @return int
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Setter for statusCode
     *
     * @param int $statusCode Value to set
     *
     * @return self
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * Set meta
     *
     * @param array $data
     * @return void
     */
    public function setMeta(array $data) : void
    {
        $this->meta = $data;
    }


    /**
     * Sets meta data to the resource
     *
     * @param object $resource
     * @return void
     */
    protected function setupMetaData($resource) : void
    {
        if (! empty($this->meta)) {
            $resource->setMeta($this->meta);
        }
    }

    /**
     * Responds with a single item
     *
     * @param array|object $item
     * @param object $callback
     * @param integer $statusCode
     * @return JsonResponse
     */
    public function respondWithItem($item, $callback, $statusCode = 200) : JsonResponse
    {
        $resource = $this->dataTransformer->getItem($item, $callback);

        $this->setupMetaData($resource);
        $this->setStatuscode($statusCode);

        $rootScope = $this->dataTransformer->createData($resource);

        return $this->respondWithArray($rootScope->toArray());
    }

    /**
     * Responds with a collection
     *
     * @param object $collection
     * @param object $callback
     * @return JsonResponse
     */
    public function respondWithCollection($collection, $callback) : JsonResponse
    {
        $resource = $this->dataTransformer->getCollection($collection, $callback);

        $this->setupMetaData($resource);
        $rootScope = $this->dataTransformer->createData($resource);

        return $this->respondWithArray($rootScope->toArray());
    }

    /**
     * Responds with a paginator
     *
     * @param LengthAwarePaginator|LengthAwarePaginatorContract $paginator
     * @param object $transformer
     * @return JsonResponse
     */
    public function respondWithPaginator($paginator, $transformer)
    {
        $rawCollection = $paginator->getCollection();

        $resource = new Collection($rawCollection, $transformer, 'data');
        $this->setupMetaData($resource);

        $resource->setPaginator($this->configurePaginator($paginator));

        $queryParams = array_diff_key($_GET, array_flip(['page']));
        $paginator->appends($queryParams);
        $rootScope = $this->dataTransformer->createData($resource);

        return $this->respondWithArray($rootScope->toArray());
    }

    public function configurePaginator($paginator)
    {
        if(get_class($paginator) === 'Illuminate\Pagination\LengthAwarePaginator') {
            return new IlluminatePaginatorAdapter($paginator);
        }

        return new IlluminateSimplePaginatorAdapter($paginator);
    }

    /**
     * Sends the response
     *
     * @param array $array
     * @return JsonResponse
     */
    protected function respondWithArray(array $array) : JsonResponse
    {
        return response()->json($array, $this->statusCode);
    }

    /**
     * Responds with error
     *
     * @param string $message
     * @param int $errorCode
     * @return JsonResponse
     */
    public function respondWithError($message, $errorCode) : JsonResponse
    {
        if ($this->statusCode === 200) {
            throw InvalidResponseCode::httpOk($this->statusCode);
        }

        return $this->respondWithArray([
            'error' => [
                'code' => $errorCode,
                'http_code' => $this->statusCode,
                'message' => $message,
            ]
        ]);
    }

    /**
     * Generates a Response with a 200 header and a given message.
     *
     * @param string $message
     * @return JsonResponse
     */
    public function respondWithSuccess($message = 'success') : JsonResponse
    {
        return $this->respondWithArray([
            'code' => self::$CODE_SUCCESS,
            'http_code' => 200,
            'message' => $message
        ]);
    }

    /**
     * Generates a Response with a 403 HTTP header and a given message.
     *
     * @param string $message
     * @return JsonResponse
     */
    public function errorForbidden($message = 'Forbidden') : JsonResponse
    {
        return $this->setStatusCode(403)
            ->respondWithError($message, self::$CODE_FORBIDDEN);
    }

    /**
     * Generates a Response with a 500 HTTP header and a given message.
     *
     * @param string $message
     * @return JsonResponse
     */
    public function errorInternalError($message = 'Internal Error') : JsonResponse
    {
        return $this->setStatusCode(500)
            ->respondWithError($message, self::$CODE_INTERNAL_ERROR);
    }

    /**
     * Generates a Response with a 404 HTTP header and a given message.
     *
     * @param string $message
     * @return JsonResponse
     */
    public function errorNotFound($message = 'Resource Not Found') : JsonResponse
    {
        return $this->setStatusCode(404)
            ->respondWithError($message, self::$CODE_NOT_FOUND);
    }

    /**
     * Generates a Response with a 401 HTTP header and a given message.
     *
     * @param string $message
     * @return JsonResponse
     */
    public function errorUnauthorized($message = 'Unauthorized') : JsonResponse
    {
        return $this->setStatusCode(401)
            ->respondWithError($message, self::$CODE_UNAUTHORIZED);
    }

    /**
     * Generates a Response with a 400 HTTP header and a given message.
     *
     * @param string $message
     * @return JsonResponse
     */
    public function errorWrongArgs($message = 'Wrong Arguments') : JsonResponse
    {
        return $this->setStatusCode(400)
          ->respondWithError($message, self::$CODE_WRONG_ARGS);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function eagerLoad(Request $request) : array
    {
        $this->requestedIncludes = explode(',', $request->input('include'));
        $eagerLoad = array_keys(array_intersect($this->possibleRelationships, $this->requestedIncludes));

        return $eagerLoad;
    }

    /**
     * Returns an array with allowed relationships
     *
     * @param array $allowedRelationships
     * @return array
     */
    public function getEagerLoad($allowedRelationships = []) : array
    {
        $eagerLoad = array_intersect($this->dataTransformer->getRequestedIncludes(), $allowedRelationships);

        return $eagerLoad;
    }


    /**
    * @return array|string
    */
    public function permittedFields()
    {
        $fields = array_intersect($this->availableFields, $this->requestedFieldsets['data']);
        if (! count($fields)) {
            $fields = '*';
        }
        return $fields;
    }
}
