<?php

namespace Infab\Core;

use Illuminate\Support\ServiceProvider;

class CoreServiceProvider extends ServiceProvider
{
    /**
     * Boot the service provider.
     *
     * @return void
     */
    public function boot()
    {
        // $this->publishes([
        //     __DIR__.'/config/iqcmscore.php' => config_path('iqcmscore.php'),
        // ]);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->commands([
            \Infab\Core\Console\TransformerMakeCommand::class
        ]);
        $this->commands([
            \Infab\Core\Console\ViewComposerMakeCommand::class
        ]);
        $this->commands([
            \Infab\Core\Console\ApiControllerMakeCommand::class
        ]);
        $this->commands([
            \Infab\Core\Console\ApiControllerTestMakeCommand::class
        ]);
        $this->commands([
            \Infab\Core\Console\RepositoryMakeCommand::class
        ]);
        $this->commands([
            \Infab\Core\Console\RepositoryInterfaceMakeCommand::class
        ]);
        $this->commands([
            \Infab\Core\Console\EloquentRepositoryMakeCommand::class
        ]);
        $this->commands([
            \Infab\Core\Console\CachingRepositoryMakeCommand::class
        ]);
    }
}
