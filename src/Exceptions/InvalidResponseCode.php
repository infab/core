<?php
namespace Infab\Core\Exceptions;

use Exception;

class InvalidResponseCode extends Exception
{
    public static function httpOk(int $code)
    {
        return new static("You really shouldn't return an error with response code \"{$code}\"");
    }
}
